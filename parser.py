from ply import yacc

import tree

from lexer import Lexer


class Coord:
    __slots__ = ('file', 'line', 'column')

    def __init__(self, file, line, column=None):
        self.file = file
        self.line = line
        self.column = column

    def __str__(self):
        str = "%s:%s" % (self.file, self.line)
        if self.column: str += ":%s" % self.column
        return str


class ParseError(Exception):
    pass


class Parser:
    def __init__(
            self,
            lexer=Lexer,
            yacc_debug=True,
            taboutputdir=''):

        self.lex = lexer(
            error_func=self._lex_error_func,
            on_lbrace_func=self._lex_on_lbrace_func,
            on_rbrace_func=self._lex_on_rbrace_func,
            type_lookup_func=self._lex_type_lookup_func)

        self.lex.build(

            # lextab=lextab,
            outputdir=taboutputdir)
        self.tokens = self.lex.tokens

        self.parser = yacc.yacc(
            module=self,
            start='translation_unit_or_empty',
            debug=yacc_debug,
            outputdir=taboutputdir)

        self._scope_stack = [dict()]

        self._last_yielded_token = None

        self.func_dict = {}
        self._err_flag = False

        self._move_tuple = ('up', 'down', 'left', 'right')
        self._robot_operators = tuple('move' + el for el in self._move_tuple) + tuple(
            'ping' + el for el in self._move_tuple)

    def _coord(self, lineno, column=None):
        return Coord(
            file=self.lex.filename,
            line=lineno,
            column=column)

    def _token_coord(self, p, token_idx):
        # print(p.lexpos(token_idx))
        last_cr = p.lexer.lexer.lexdata.rfind('\n', 0, p.lexpos(token_idx))
        if last_cr < 0:
            last_cr = -1
        column = (p.lexpos(token_idx) - (last_cr))
        return self._coord(p.lineno(token_idx), column)

    def _parse_error(self, msg, coord):
        raise ParseError("%s: %s" % (coord, msg))

    def parse(self, text, filename='', debuglevel=True):
        self.lex.filename = filename
        self.lex.reset_lineno()
        self._scope_stack = [dict()]
        self._last_yielded_token = None
        return self.parser.parse(
            input=text,
            lexer=self.lex,
            debug=debuglevel)

    def _push_scope(self):
        self._scope_stack.append(dict())

    def _pop_scope(self):
        assert len(self._scope_stack) > 1
        self._scope_stack.pop()

    def _add_typedef_name(self, name, coord):
        if not self._scope_stack[-1].get(name, True):
            self._parse_error(
                "Typedef %r previously declared as non-typedef "
                "in this scope" % name, coord)
        self._scope_stack[-1][name] = True

    def _add_identifier(self, name, coord):
        if self._scope_stack[-1].get(name, False):
            self._parse_error(
                "Non-typedef %r previously declared as typedef "
                "in this scope" % name, coord)
        self._scope_stack[-1][name] = False

    def _is_type_in_scope(self, name):

        for scope in reversed(self._scope_stack):
            # If name is an identifier in this scope it shadows typedefs in
            # higher scopes.
            in_scope = scope.get(name)
            if in_scope is not None: return in_scope
        return False

    def _lex_error_func(self, msg, line, column):
        self._parse_error(msg, self._coord(line, column))

    def _lex_on_lbrace_func(self):
        self._push_scope()

    def _lex_on_rbrace_func(self):
        self._pop_scope()

    def _lex_type_lookup_func(self, name):
        is_type = self._is_type_in_scope(name)
        return is_type

    def _get_yacc_lookahead_token(self):
        return self.lex.last_token

    def _type_modify_decl(self, decl, modifier):

        modifier_head = modifier
        modifier_tail = modifier

        while modifier_tail.type:
            modifier_tail = modifier_tail.type

        if isinstance(decl, tree.TypeDecl):
            modifier_tail.type = decl
            return modifier
        else:
            decl_tail = decl

            while not isinstance(decl_tail.type, tree.TypeDecl):
                decl_tail = decl_tail.type

            modifier_tail.type = decl_tail.type
            decl_tail.type = modifier_head
            return decl

    def _fix_decl_name_type(self, decl, typename):

        # Reach the underlying basic type
        #
        type = decl
        while not isinstance(type, tree.TypeDecl):
            type = type.type

        decl.name = type.declname

        if typename:
            for tn in typename:
                if not isinstance(tn, tree.IdentifierType):
                    if len(typename) > 1:
                        self._parse_error(
                            "Invalid multiple types specified", tn.coord)
                    else:
                        tn = tn['type'][0]  # TODO
                        type.type = tn
                        return decl

        if not typename:

            type.type = tree.IdentifierType(
                ['None'],
                coord=decl.coord)
        else:
            # At this point, we know that typename is a list of IdentifierType
            # nodes. Concatenate all the names into a single list.
            #
            type.type = tree.IdentifierType(
                [name for id in typename for name in id.names],
                coord=typename[0].coord)
        return decl

    def _add_declaration_specifier(self, declspec, newspec, kind, append=False):
        spec = declspec or dict(qual=[], storage=[], type=[], function=[])

        if append:
            spec[kind].append(newspec)
        else:
            spec[kind].insert(0, newspec)

        return spec

    def _build_declarations(self, spec, decls, typedef_namespace=False):

        declarations = []

        if decls[0]['decl'] is None:

            if len(spec['type']) < 2 or len(spec['type'][-1].names) != 1 or \
                    not self._is_type_in_scope(spec['type'][-1].names[0]):
                coord = '?'
                for t in spec['type']:
                    if hasattr(t, 'coord'):
                        coord = t.coord
                        break
                self._parse_error('Invalid declaration', coord)


            decls[0]['decl'] = tree.TypeDecl(
                declname=spec['type'][-1].names[0],
                type=None,

                coord=spec['type'][-1].coord)

            del spec['type'][-1]

        elif not isinstance(decls[0]['decl'],
                            (tree.Struct, tree.IdentifierType)):
            decls_0_tail = decls[0]['decl']
            while not isinstance(decls_0_tail, tree.TypeDecl):
                decls_0_tail = decls_0_tail.type
            if decls_0_tail.declname is None:
                decls_0_tail.declname = spec['type'][-1].names[0]
                del spec['type'][-1]

        for decl in decls:
            assert decl['decl'] is not None
            conv_to = None
            conv_from = None
            if spec is not None:
                conv_to = spec.get('conv_to')
                conv_from = spec.get('conv_from')
            declaration = tree.Decl(
                name=None,
                type=decl['decl'],
                init=decl.get('init'),
                coord=decl['decl'].coord,
                conv_to=conv_to,
                conv_from=conv_from,
                )

            if isinstance(declaration.type,
                          (tree.Struct, tree.IdentifierType)):
                fixed_decl = declaration
            else:
                if spec:
                    fixed_decl = self._fix_decl_name_type(declaration, spec['type'])
                else:
                    fixed_decl = self._fix_decl_name_type(declaration, None)

            if typedef_namespace:
                self._add_identifier(fixed_decl.name, fixed_decl.coord)

            declarations.append(fixed_decl)

        return declarations

    def _build_function_definition(self, spec, decl, param_decls, body):
        declaration = self._build_declarations(
            spec=spec,
            decls=[dict(decl=decl, init=None)],
            typedef_namespace=True)[0]

        return tree.FuncDef(
            decl=declaration,
            param_decls=param_decls,
            body=body,
            coord=decl.coord)

    precedence = (
        ('right', 'EQUALS'),
        ('left', 'EQ', 'NE'),
        ('left', 'GT', 'GE', 'LT', 'LE'),
        ('left', 'DOT'),
        ('left', 'PLUS', 'MINUS'),
        ('left', 'TIMES', 'DIVIDE'),
        ('right', 'POWER')
    )

    def p_translation_unit_or_empty(self, p):
        """ translation_unit_or_empty   : translation_unit
                                        | empty
        """
        if p[1] is None:
            p[0] = tree.FileAST([])
        else:
            p[0] = tree.FileAST(p[1])

    def p_translation_unit_1(self, p):
        """ translation_unit    : external_declaration
        """

        p[0] = p[1]

    def p_translation_unit_2(self, p):
        """ translation_unit    : translation_unit external_declaration
        """
        if type(p[2]) == list:
            p[1].extend(p[2])
        else:
            p[1].append(p[2])
        p[0] = p[1]

    def p_external_declaration_1(self, p):
        """ external_declaration    : function_definition
                                    | statement
        """
        p[0] = [p[1]]

    def p_external_declaration_2(self, p):
        """ external_declaration    : declaration

        """  # объвление переменной
        p[0] = p[1]

    def p_external_declaration_4(self, p):
        """ external_declaration    : COMMA
        """
        p[0] = []

    def p_function_definition(self, p):
        """ function_definition : PROC id_declarator declaration_list compound_statement
                                | PROC id_declarator empty compound_statement

        """

        p[0] = self._build_function_definition(
            spec=None,
            decl=p[2],
            param_decls=p[3],
            body=p[4])
        if self.func_dict.get(p[0].decl.name) and self.func_dict.get(p[0].decl.name) != "def":
            print("ERROR at %s : function definition is already exists" % self._coord(p[0].coord))
            self._err_flag = True
        else:
            self.func_dict[p[0].decl.name] = p[0]

    def p_statement(self, p):
        """ statement   : expression_statement
                        | compound_statement
                        | selection_statement
                        | iteration_statement
        """
        p[0] = p[1]

    def p_declaration_1(self, p):
        """declaration       : type_specifier empty COMMA

        """
        spec = p[1]

        if p[2] is None:
            ty = spec['type']

            if len(ty) == 1 and isinstance(ty[0], tree.Struct):
                decls = [tree.Decl(
                    name=None,

                    type=ty[0],
                    init=None,

                    coord=ty[0].coord)]


            else:
                decls = self._build_declarations(
                    spec=spec,
                    decls=[dict(decl=None, init=None)],
                    typedef_namespace=True)



        else:
            decls = self._build_declarations(
                spec=spec,
                decls=p[2],
                typedef_namespace=True)

        p[0] = decls

    def p_declaration(self, p):
        """ declaration       : type_specifier init_declarator COMMA

        """
        p[2] = [p[2]]
        spec = p[1]
        if p[2] is None:
            if type(p[1]) == tree.Struct:
                ty = spec

                decls = [tree.Decl(
                    name=None,

                    type=ty,
                    init=None,

                    coord=ty.coord)]
            else:
                decls = self._build_declarations(
                    spec=spec,
                    decls=[dict(decl=None, init=None)],
                    typedef_namespace=True)

        else:
            decls = self._build_declarations(
                spec=spec,
                decls=p[2],
                typedef_namespace=True)

        p[0] = decls

    def p_declaration_list(self, p):
        """ declaration_list    : declaration
                                | declaration_list declaration
        """
        p[0] = p[1] if len(p) == 2 else p[1] + p[2]

    def p_type_specifier(self, p):
        """ type_specifier            : LOGIC
                                      | NUMERIC
                                      | STRING
                                      | typedef_name
                                      | ARRAY
        """

        buf = tree.IdentifierType([p[1]], coord=self._token_coord(p, 1))
        p[0] = self._add_declaration_specifier(None, buf, 'type')

    def p_type_specifier_1(self, p):
        """ type_specifier  : struct_specifier
        """
        p[0] = p[0] = self._add_declaration_specifier(None, p[1], 'type')

    def p_initializer_1(self, p):
        """ initializer : assignment_expression
        """
        p[0] = p[1]

    def p_init_declarator(self, p):
        """ init_declarator : declarator
                            | declarator EQUALS initializer
        """
        p[0] = dict(decl=p[1], init=(p[3] if len(p) > 2 else None))

    def p_declarator(self, p):
        """ declarator  : id_declarator
        """
        p[0] = p[1]

    def p_id_declarator_1(self, p):
        """ id_declarator  : direct_id_declarator
        """
        p[0] = p[1]

    def p_direct_id_declarator_1(self, p):
        """ direct_id_declarator   : ID
        """
        p[0] = tree.TypeDecl(
            declname=p[1],
            type=None,

            coord=self._token_coord(p, 1))

    def p_direct_id_declarator_2(self, p):
        """ direct_id_declarator   : LPAREN id_declarator RPAREN
        """
        p[0] = p[2]

    def p_direct_id_declarator_3(self, p):
        """ direct_id_declarator   : direct_id_declarator LPAREN parameter_type_list RPAREN

                                   | direct_id_declarator LPAREN empty RPAREN
        """

        func = tree.FuncDecl(
            args=p[3],
            type=None,
            coord=p[1].coord)

        self.func_dict[p[1].declname] = "def"
        if self._get_yacc_lookahead_token().type == "LBRACE":
            if func.args is not None:
                for param in func.args.params:
                    self._add_identifier(param.name, param.coord)

        f = self._type_modify_decl(decl=p[1], modifier=func)
        p[0] = f

    def p_direct_id_declarator_4(self, p):
        """ direct_id_declarator   : direct_id_declarator LBRACKET assignment_expression RBRACKET
                                   | direct_id_declarator LBRACKET empty RBRACKET
        """
        quals = (p[3] if len(p) > 5 else []) or []

        if p[3] is None:
            print("Error at {}:{}: empty index occured".format(p[1].coord.line, p[1].coord.column + 1))
            self._err_flag = True
        arr = tree.ArrayDecl(
            type=None,
            dim=p[4] if len(p) > 5 else p[3],

            coord=p[1].coord)
        # TODO error when empty
        p[0] = self._type_modify_decl(decl=p[1], modifier=arr)

    def p_parameter_type_list(self, p):
        """ parameter_type_list : parameter_list
        """
        p[0] = p[1]

    def p_parameter_list(self, p):
        """ parameter_list  : parameter_declaration
                            | parameter_list COMMA parameter_declaration
        """
        if len(p) == 2:  # single parameter
            p[0] = tree.ParamList([p[1]], p[1].coord)
        else:
            p[1].params.append(p[3])
            p[0] = p[1]

    def p_parameter_declaration(self, p):
        """ parameter_declaration   : type_specifier id_declarator
        """

        spec = p[1]

        p[0] = self._build_declarations(
            spec=spec,
            decls=[dict(decl=p[2])])[0]

    def p_parameter_declaration_1(self, p):
        """ parameter_declaration   : type_specifier  id_declarator AMPERSAND
        """

        spec = p[1]
        if type(spec['type'][0].names[0]) == tree.IdentifierType:
            spec['type'][0].names[0] = spec['type'][0].names[0].names[0]
        spec['type'] = [tree.IdentifierType([spec['type'][0].names[0] + '&'],
                                            coord=self._token_coord(p, 1)
                                            )]

        p[0] = self._build_declarations(
            spec=spec,
            decls=[dict(decl=p[2])])[0]

    def p_block_item(self, p):
        """ block_item  : declaration
                        | statement
        """
        p[0] = p[1] if isinstance(p[1], list) else [p[1]]

    def p_block_item_list(self, p):
        """ block_item_list : block_item
                            | block_item_list block_item
        """
        p[0] = p[1] if (len(p) == 2 or p[2] == [None]) else p[1] + p[2]

    def p_compound_statement_1(self, p):
        """ compound_statement : brace_open block_item_list brace_close
                               | brace_open empty brace_close
        """
        p[0] = tree.Compound(
            block_items=p[2],
            coord=self._token_coord(p, 1))

    def p_selection_statement_1(self, p):
        """ selection_statement : IF expression statement
                                | IF empty statement
        """
        if p[2] is None:
            print("ERROR at {}:{} : wrong format of condition".format(p[3].coord.line, 10))
            self._err_flag = True
        p[0] = tree.If(p[2], p[3], None, self._token_coord(p, 1))

    def p_iteration_statement_1(self, p):
        """ iteration_statement : OPEN_BRACKET expression CLOSE_BRACKET statement
        """
        p[0] = tree.While(p[2], p[4], self._token_coord(p, 1))

    def p_expression_statement(self, p):
        """ expression_statement : expression COMMA
                                 | empty COMMA
        """

        if p[1] is None:
            p[0] = tree.EmptyStatement(self._token_coord(p, 2))
        else:
            p[0] = p[1]

    def p_expression(self, p):
        """ expression  : assignment_expression

        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            if not isinstance(p[1], tree.ExprList):
                p[1] = tree.ExprList([p[1]], p[1].coord)

            p[1].exprs.append(p[3])
            p[0] = p[1]

    def p_assignment_expression(self, p):
        """ assignment_expression   : binary_expression
                                    | unary_expression assignment_operator initializer
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = tree.Assignment(p[2], p[1], p[3], p[1].coord)

    def p_assignment_operator(self, p):
        """ assignment_operator : EQUALS

        """
        p[0] = p[1]

    def p_binary_expression(self, p):
        """ binary_expression   : cast_expression
                                | binary_expression POWER binary_expression
                                | binary_expression TIMES binary_expression
                                | binary_expression DIVIDE binary_expression
                                | binary_expression PLUS binary_expression
                                | binary_expression MINUS binary_expression
                                | binary_expression LT binary_expression
                                | binary_expression LE binary_expression
                                | binary_expression GE binary_expression
                                | binary_expression GT binary_expression
                                | binary_expression EQ binary_expression
                                | binary_expression NE binary_expression


        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = tree.BinaryOp(p[2], p[1], p[3], p[1].coord)

    def p_binary_expression_1(self, p):
        """ binary_expression   : binary_expression DOT POWER binary_expression
                                | binary_expression DOT TIMES binary_expression
                                | binary_expression DOT DIVIDE binary_expression
                                | binary_expression DOT PLUS binary_expression
                                | binary_expression DOT MINUS binary_expression
                                | binary_expression DOT LT binary_expression
                                | binary_expression DOT LE binary_expression
                                | binary_expression DOT GE binary_expression
                                | binary_expression DOT GT binary_expression
                                | binary_expression DOT EQ binary_expression
                                | binary_expression DOT NE binary_expression


        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = tree.BinaryOp('.' + p[3], p[1], p[4], p[1].coord)

    def p_binary_expression_2(self, p):
        """ binary_expression   : binary_expression POWER  DOT binary_expression
                                | binary_expression TIMES  DOT binary_expression
                                | binary_expression DIVIDE DOT binary_expression
                                | binary_expression PLUS   DOT binary_expression
                                | binary_expression MINUS  DOT binary_expression
                                | binary_expression LT     DOT binary_expression
                                | binary_expression LE     DOT binary_expression
                                | binary_expression GE     DOT binary_expression
                                | binary_expression GT     DOT binary_expression
                                | binary_expression EQ     DOT binary_expression
                                | binary_expression NE     DOT binary_expression


        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = tree.BinaryOp(p[2] + '.', p[1], p[4], p[1].coord)

    def p_cast_expression_1(self, p):
        """ cast_expression : unary_expression
        """
        p[0] = p[1]

    def p_unary_expression_1(self, p):
        """ unary_expression    : postfix_expression """
        p[0] = p[1]

    def p_unary_expression_2(self, p):
        """ unary_expression    : unary_operator cast_expression
        """
        p[0] = tree.UnaryOp(p[1], p[2], p[2].coord)

    def p_unary_expression_3(self, p):
        """ unary_expression    : UNDEF unary_expression

        """
        p[0] = tree.UnaryOp(
            p[1],
            p[2] if len(p) == 3 else p[3],
            self._token_coord(p, 1))

    def p_unary_operator(self, p):
        """ unary_operator  : PLUS
                            | MINUS
                            | UNDEF
        """

        p[0] = p[1]

    def p_postfix_expression_1(self, p):
        """ postfix_expression  : identifier
                                | constant
        """
        p[0] = p[1]

    def p_postfix_expression_2(self, p):
        """ postfix_expression  : postfix_expression LPAREN argument_expression_list RPAREN
                                | postfix_expression LPAREN RPAREN
        """
        if self.func_dict.get(p[1].name) is None and p[1].name not in self._robot_operators:
            if p[1].name not in ('print', 'vision', 'voice'):
                print("ERROR at {} : function {} is not already defined".format(self._coord(p[1].coord), p[1].name))
                self._err_flag = True

        p[0] = tree.FuncCall(p[1], p[3] if len(p) == 5 else None, p[1].coord)

    def p_postfix_expression_3(self, p):
        """ postfix_expression  : postfix_expression LBRACKET expression RBRACKET
                                | postfix_expression LBRACKET empty RBRACKET
        """
        if p[3] is None:
            print("ERROR at {}:{} : empty index occured".format(p[1].coord.line, p[1].coord.column + 1))
            self._err_flag = True
        p[0] = tree.ArrayRef(p[1], p[3], p[1].coord)

    def p_postfix_expression_4(self, p):
        """ postfix_expression  : LPAREN expression RPAREN """
        p[0] = p[2]

    def p_postfix_expression_5(self, p):
        """ postfix_expression  : postfix_expression ARROW ID


        """
        field = tree.ID(p[3], self._token_coord(p, 3))
        p[0] = tree.StructRef(p[1], p[2], field, p[1].coord)

    def p_argument_expression_list(self, p):
        """ argument_expression_list    : assignment_expression
                                        | argument_expression_list COMMA assignment_expression
        """
        if len(p) == 2:  # single expr
            p[0] = tree.ExprList([p[1]], p[1].coord)
        else:
            p[1].exprs.append(p[3])
            p[0] = p[1]

    def p_identifier(self, p):
        """ identifier  : ID """
        p[0] = tree.ID(p[1], self._token_coord(p, 1))

    def p_constant_1(self, p):
        """ constant    : INT_CONST

        """
        p[0] = tree.Constant(
            'numeric', p[1], self._token_coord(p, 1))

    def p_constant_2(self, p):
        """ constant    : FALSE
        """

        p[0] = tree.Constant(
            'logic', False, self._token_coord(p, 1))

    def p_constant_3(self, p):
        """ constant    : TRUE
        """

        p[0] = tree.Constant(
            'logic', True, self._token_coord(p, 1))

    def p_constant_4(self, p):
        """ constant    : CHAR_CONST
        """
        p[0] = tree.Constant(
            'string', p[1], self._token_coord(p, 1))

    def p_constant_5(self, p):
        """ constant    : STRING_LITERAL
        """
        p[0] = tree.Constant(
            'string', p[1], self._token_coord(p, 1))

    def p_brace_open(self, p):
        """ brace_open  :   BLOCK
        """
        p[0] = p[1]
        p.set_lineno(0, p.lineno(1))

    def p_brace_close(self, p):
        """ brace_close :   UNBLOCK
        """
        p[0] = p[1]
        p.set_lineno(0, p.lineno(1))

    def p_struct_specifier(self, p):
        """ struct_specifier   : struct ID DATA brace_open struct_declaration_list brace_close
                               | struct ID DATA brace_open brace_close
        """

        if len(p) == 6:
            # Empty sequence means an empty list of members
            p[0] = tree.Struct(
                name=p[2],
                decls=[],
                coord=self._token_coord(p, 2))
        else:
            p[0] = tree.Struct(
                name=p[2],
                decls=p[5],
                coord=self._token_coord(p, 2))

        self._add_typedef_name(p[2], self._token_coord(p, 2))

    def p_struct_declarator(self, p):
        """ struct_declarator : declarator
        """
        p[0] = {'decl': p[1]}

    def p_struct_declarator_list(self, p):
        """ struct_declarator_list  : struct_declarator
        """
        p[0] = [p[1]]

    def p_struct_declaration_list(self, p):
        """ struct_declaration_list     : struct_declaration
                                        | struct_declaration_list struct_declaration
        """
        if len(p) == 2:
            p[0] = p[1] or []
        else:
            p[0] = p[1] + (p[2] or [])

    def p_conversion_list(self, p):
        """
        conversion_list : conversion_list conversion_declaration
                        | conversion_declaration
                        | empty
        """
        if len(p) == 2:  # single parameter
            if p[1]:
                p[0] = [p[1]]
        else:
            p[1].append(p[2])
            p[0] = p[1]

    def p_conversion_declaration(self, p):
        """
        conversion_declaration : CONVERSION TO parameter_declaration
        """
        p[0] = ['to', p[3].type.declname, p[3].type.type.names[0]]

    def p_conversion_declaration_1(self, p):
        """
        conversion_declaration : CONVERSION FROM parameter_declaration
        """
        p[0] = ['from', p[3].type.declname, p[3].type.type.names[0]]

    def p_struct_declaration(self, p):
        """ struct_declaration : specifier_qualifier struct_declarator_list conversion_list COMMA
                               | specifier_qualifier empty COMMA
        """
        spec = p[1]
        assert 'typedef' not in spec['storage']

        if p[2] is not None:

            if p[3] is not None:
                spec["conv_to"] = {}
                spec["conv_from"] = {}
                for item in p[3]:
                    if item[0] == "to":
                        if spec["conv_to"].get(item[2]) is not None:
                            print("Error at {}: conversion to {} is already exists".format(self._token_coord(p, 1), item[2]))
                            self._err_flag = True
                        elif self.func_dict.get(item[1]) is None:
                            print("Error at {}: function {} does not exist".format(self._token_coord(p, 1), item[1]))
                            self._err_flag = True
                        else:
                            spec["conv_to"][item[2]] = item[1]
                    else:
                        if spec["conv_from"].get(item[2]) is not None:
                            print("Error at {}: conversion from {} is already exists".format(self._token_coord(p, 1), item[2]))
                            self._err_flag = True
                        else:
                            spec["conv_from"][item[2]] = item[1]


            decls = self._build_declarations(
                spec=spec,
                decls=p[2])

        else:
            node = spec['type'][0]
            if isinstance(node, tree.Node):
                decl_type = node
            else:
                decl_type = tree.IdentifierType(node)

            decls = self._build_declarations(
                spec=spec,
                decls=[dict(decl=decl_type)])

            print("Error at {}: missing field".format(self._token_coord(p, 1)))
            self._err_flag = True

        p[0] = decls

    def p_specifier_qualifier(self, p):
        """ specifier_qualifier  : type_specifier
        """
        p[0] = self._add_declaration_specifier(None, p[1], 'type')

    def p_struct(self, p):
        """ struct : RECORD

        """
        p[0] = p[1]

    def p_typedef_name(self, p):
        """ typedef_name : TYPEID """
        p[0] = tree.IdentifierType([p[1]], coord=self._token_coord(p, 1))

    def p_empty(self, p):
        """empty : """
        p[0] = None

    def p_error(self, p):

        if p:
            self._parse_error(
                'before: %s' % p.value,
                self._coord(lineno=p.lineno,
                            column=self.lex.find_tok_column(p)))
        else:
            self._parse_error('At end of input', self.lex.filename)
