from parser import Parser
import numpy as np
from nodevisitor import NodeVisitor

# import sys, os
#
# my_path = os.path.dirname(os.path.abspath(__file__))
#
# sys.path.insert(0, my_path + '/../')


def get_ast(text, parser):
    return parser.parse(text, filename='<none>', debuglevel=False)


def get_value(exec, name, scope_name='__global'):
    return exec.scopes[scope_name][name][1]


def run_test(text):
    parser = Parser(yacc_debug=True)
    ast = get_ast(text, parser)
    exec = NodeVisitor(parser._err_flag, parser.func_dict)
    exec.visit(ast)
    return exec


def test_conditions():
    text = r"""
        numeric t = 2,

        {t ? 2} block
            t = t + 2,
            t = t ^ 2,
        unblock,

        if (t ? 4)
            t = t + 2,

        t = t / 2,
    """

    exec = run_test(text)
    assert exec.scopes['__global']['t'][1] == 8


def test_array_decls_records():
    text = r"""
        undef "qwe",
        record ttt data block
            string t[6][3][4],
        unblock,
        
        ttt test, 
    """

    exec = run_test(text)
    t = get_value(exec, 'test')
    assert np.array_equal(t['t'][1], np.full((6, 3, 4), '"qwe"'))


def test_array_decls_simple():
    text = r"""
        undef 10,
        numeric r = 3,
        numeric t[r][4][2],
    """

    exec = run_test(text)
    t = get_value(exec, 't')
    assert np.array_equal(t, np.full((3, 4, 2), 10))


def test_logic_operators():
    text = r"""
        logic t = false,
        logic m = t,
        m = m ^ t,
        t = m / t,
    """

    exec = run_test(text)
    m = get_value(exec, 'm')
    assert m is True

    t = get_value(exec, 't')
    assert t is True


def test_struct_array_ref():
    text = r"""
        record my_array data block
            numeric arr[4],
        unblock,

        record my_type data block
            my_array fff[2][2],
            numeric val,
            string vvv,
        unblock,

        my_type zzz,
        numeric check[4] = zzz->fff[0][1]->arr,
        logic t = zzz->fff[0][1]->arr[2], 
    """

    exec = run_test(text)
    zzz = get_value(exec, 'zzz')
    for item in zzz['fff'][1]:
        for el in item:
            assert np.array_equal(el['arr'][1], np.array([0, 0, 0, 0]))

    check = get_value(exec, 'check')
    assert np.array_equal(check, np.array([0, 0, 0, 0]))

    t = get_value(exec, 't')
    assert t is False


def test_array_operations():
    text = r"""
        undef 4,

        numeric t[4][3],

        undef 10,
        numeric b[4][3],

        t = t + b,
    """

    exec = run_test(text)
    t = get_value(exec, 't')
    assert np.array_equal(t, np.full((4, 3), 14))

    b = get_value(exec, 'b')
    assert np.array_equal(b, np.full((4, 3), 10))

def test_casts():
    text = r"""
        undef true,
        logic t,
        
        t = 4 +. false, 
    """

    exec = run_test(text)
    t = get_value(exec, 't')
    assert t is True

def test_recursion():
    text = r"""
        numeric n = 5,
        numeric buf = 0,
        numeric res = 1,
        proc factorial() block
            buf = n,
            if (buf ! 1) block
                res = res * buf,
                buf = buf - 1,
                n = buf,
                factorial(),
            unblock,
        unblock,

        factorial(),
    """

    exec = run_test(text)

    n = get_value(exec, 'n')
    buf = get_value(exec, 'buf')
    res = get_value(exec, 'res')

    assert n == 1 and buf == 1 and res == 120


def test_assignment():
    text = r"""
        numeric a = 1,
        numeric b = 2,
        numeric c = 3,

        a = b = c,
    """

    exec = run_test(text)

    a = get_value(exec, 'a')
    b = get_value(exec, 'b')
    c = get_value(exec, 'c')

    assert a == b == c == 3


def test_references():
    text = r"""
        proc inc(numeric n &) block
            n = n + 1,
        unblock,

        proc test(numeric d &) block
            numeric fff = 3,

            inc(fff),

            d = d + fff,
        unblock

        proc test_no_reference(numeric d) block
            numeric fff = 3,

            inc(fff),

            d = d + fff,
        unblock

        numeric d = 2,
        test(d),

        test_no_reference(d),
        print(d),
    """

    exec = run_test(text)
    d = get_value(exec, 'd')
    assert d == 6


def test_robot_operators():
    text = r"""
        undef "undef",

        record robot_position data block
            numeric x,
            numeric y,
        unblock,

        robot_position pos,

        proc init(robot_position pos &) block
            pos->x = 1,
            pos->y = 7,
        unblock

        init(pos),

        if (map[pos->y][pos->x] ! 0)
            print("ERROR", map[pos->y][pos->x]),

        undef "",

        string r[4],

        numeric buf = 1,
        movedown(pos, buf),

        vision(pos, r),
        voice(pos, r[2]),

        numeric pos_x = pos->x,
        numeric pos_y = pos->y,
    """

    exec = run_test(text)
    buf = get_value(exec, 'buf')
    assert buf == 0

    pos_x = get_value(exec, 'pos_x')
    assert pos_x == -1

    pos_y = get_value(exec, 'pos_y')
    assert pos_y == -1


def test_class_stack():
    text = r"""
        record stack data block
            numeric _data[10],
            numeric head, 
        unblock,

        proc push(stack st&, numeric num) block
            st->_data[st->head] = num,
            st->head = st->head + 1, 
        unblock

        proc pop(stack st&, numeric res&) block
            if (st->head ? 0)
                print("ERROR: stack is empty"),
            if (st->head ! 0) block
                st->head = st->head - 1,
                res = st->_data[st->head],
            unblock,
        unblock

        stack test,

        push(test, 666),
        push(test, 123),
        push(test, 321),
        numeric res1,
        numeric res2,
        numeric res3,
        
        numeric res4 = 100,
        pop(test, res1),
        pop(test, res2),
        pop(test, res3),
        pop(test, res4),
    """

    exec = run_test(text)

    res1 = get_value(exec, 'res1')
    assert res1 == 321

    res2 = get_value(exec, 'res2')
    assert res2 == 123

    res3 = get_value(exec, 'res3')
    assert res3 == 666

    res4 = get_value(exec, 'res4')
    assert res4 == 100


def test_conversions_to():
    text = r"""
        undef true,

        proc conv_val_to_num(array _from &, numeric _to &) block
            _to = _from[0]  *. 1,
        unblock,

        record my_type data block

            logic val[4] conversion to numeric conv_val_to_num,

        unblock,

        my_type test,

        numeric t =  4 .+ test->val,
    """
    exec = run_test(text)

    t = get_value(exec, 't')
    assert t == 5


def test_conversons_from():
    text = r"""
        undef 10,

        record my_type data block
            numeric val[4] conversion from string conv_string_to_val,
        unblock,

        proc conv_string_to_val(string _from &, my_type _to &) block
            numeric cnt = 0,
            {cnt ! 4} block
                _to->val[cnt] = _from *. 1,
                cnt = cnt + 1,
            unblock,
        unblock,

        my_type test,

        logic t[4] =  test->val .+ "10",
    """
    exec = run_test(text)

    t = get_value(exec, 't')
    np.array_equal(t, np.array([20, 20, 20, 20]))


def test_struct_conversions():
    text = r"""
        undef 10,

        proc conv_val_to_num(logic _from &, numeric _to &) block
            _to = _from  *. 1,
        unblock,

        record my_type data block
            numeric val conversion from string conv_string_to_val,
            logic boolean conversion to numeric conv_val_to_num, 
        unblock,

        proc conv_string_to_val(string _from &, my_type _to &) block
            _to->val = _from *. 1,
        unblock,

        my_type test,
        my_type test1,

        logic t =  test1->val .+ test->boolean,
    """

    exec = run_test(text)
    t = get_value(exec, 't')
    assert t == True
