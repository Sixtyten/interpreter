import sys

from ply import lex
from ply.lex import TOKEN


class Lexer:
    def __init__(self, error_func, on_lbrace_func, on_rbrace_func,
                 type_lookup_func):

        self.error_func = error_func
        self.on_lbrace_func = on_lbrace_func
        self.on_rbrace_func = on_rbrace_func
        self.type_lookup_func = type_lookup_func
        self.filename = ''

        self.last_token = None

    def build(self, **kwargs):
        self.lexer = lex.lex(object=self, **kwargs)

    def reset_lineno(self):
        self.lexer.lineno = 1

    def input(self, text):
        self.lexer.input(text)

    def token(self):
        self.last_token = self.lexer.token()
        return self.last_token

    def find_tok_column(self, token):
        last_cr = self.lexer.lexdata.rfind('\n', 0, token.lexpos)
        return token.lexpos - last_cr

    def _error(self, msg, token):
        location = self._make_tok_location(token)
        self.error_func(msg, location[0], location[1])
        self.lexer.skip(1)

    def _make_tok_location(self, token):
        return (token.lineno, self.find_tok_column(token))

    keywords = (
        'TRUE', 'FALSE', 'LOGIC', 'NUMERIC', 'STRING', 'UNDEF', 'RECORD',
        'DATA', 'BLOCK', 'UNBLOCK', 'PROC',
        'CONVERSION', 'FROM', 'TO',

        # 'MOVEUP', 'MOVEDOWN', 'MOVERIGHT',
        # 'MOVELEFT', 'PINGUP', 'PINGDOWN', 'PINGLEFT', 'PINGRIGHT', 'VISION', 'VOICE',

        # Added
        'IF', 'ARRAY'
    )

    keyword_map = {}
    for keyword in keywords:  # перевод keywords в нижний регистр
        keyword_map[keyword.lower()] = keyword

    tokens = keywords + (
        # Identifiers
        'ID',

        'TYPEID',

        # Operators
        'PLUS', 'MINUS', 'TIMES', 'DIVIDE',
        'LT', 'LE', 'GT', 'GE', 'EQ', 'NE',
        'POWER',

        # Assignment (added)
        'EQUALS',

        # Delimeters
        'LPAREN', 'RPAREN',         # ( )
        'LBRACKET', 'RBRACKET',     # [ ]
        'COMMA', 'DOT',             # , .
        'ARROW',

        'OPEN_BRACKET', 'CLOSE_BRACKET',  # { }


        'AMPERSAND',

        # Constants
        'INT_CONST',

        'STRING_LITERAL',
        'CHAR_CONST',
        'BAD_CHAR_CONST'
    )

    identifier = r'[a-zA-Z_][0-9a-zA-Z_]*'

    decimal_constant = r'0|([1-9][0-9]*)'

    simple_escape = r"""([a-zA-Z._~!=&\^\-\\?'"])"""
    decimal_escape = r"""(\d+)"""
    hex_escape = r"""(x[0-9a-fA-F]+)"""
    bad_escape = r"""([\\][^a-zA-Z._~^!=&\^\-\\?'"x0-7])"""

    escape_sequence = r"""(\\(""" + simple_escape + '|' + decimal_escape + '|' + hex_escape + '))'
    cconst_char = r"""([^'\\\n]|""" + escape_sequence + ')'
    char_const = "'" + cconst_char + "'"

    bad_char_const = r"""('""" + cconst_char + """[^'\n]+')|('')|('""" + bad_escape + r"""[^'\n]*')"""

    string_char = r"""([^"\\\n]|""" + escape_sequence + ')'
    string_literal = '"' + string_char + '*"'

    bad_string_literal = '"' + string_char + '*?' + bad_escape + string_char + '*"'

    t_ignore = ' \t'

    # Newlines
    def t_NEWLINE(self, t):
        r'\n+'
        t.lexer.lineno += t.value.count("\n")

    # Operators
    t_PLUS              = r'\+'
    t_MINUS             = r'-'
    t_TIMES             = r'\*'
    t_DIVIDE            = r'/'
    t_LT                = r'<'
    t_GT                = r'>'
    t_LE                = r'<='
    t_GE                = r'>='
    t_EQ                = r'\?' # ==
    t_NE                = r'!' # !=
    t_POWER             = r'\^' # Шеффер и степень
    t_AMPERSAND         = r'&' # ссылка



    # Assignment operator
    t_EQUALS           = r'='

    # Delimeters
    t_LPAREN            = r'\('
    t_RPAREN            = r'\)'
    t_LBRACKET          = r'\['
    t_RBRACKET          = r'\]'
    t_COMMA             = r','
    t_DOT               = r'\.'
    t_ARROW             = r'->'

    t_OPEN_BRACKET      = r'{'
    t_CLOSE_BRACKET     = r'}'

    t_STRING_LITERAL = string_literal

    @TOKEN(identifier)
    def t_ID(self, t):
        t.type = self.keyword_map.get(t.value, "ID")

        if t.type == 'ID' and self.type_lookup_func(t.value):
            t.type = "TYPEID"
        return t

    @TOKEN(decimal_constant)
    def t_INT_CONST(self, t):
        return t

    @TOKEN(char_const)
    def t_CHAR_CONST(self, t):
        return t

    @TOKEN(bad_char_const)
    def t_BAD_CHAR_CONST(self, t):
        msg = "Invalid char constant %s" % t.value
        self._error(msg, t)

    def t_error(self, t):
        msg = 'Illegal character %s' % repr(t.value[0])
        # print(msg)
        # self.lexer.skip(1)
        self._error(msg, t)
