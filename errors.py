
from parser import Parser

import nodevisitor

text = r"""
    logic t = true,
    
    numeric t,
"""

# text = r"""
#     numeric t[],
# """
#
# text = r"""
#     numeric t[5],
#
#     t[] = 4,
# """
#
# text = r"""
#     numeric t[4],
#     numeric r[5] = t[1],
# """
#
# text = r"""
#     numeric a[4],
#     numeric b[2],
#     a = a + b,
# """
#
# text = r"""
#     numeric t[3][4],
#     t[2][4][6] = "qwe",
# """

# text = r"""
#     factorial(),
# """
#
# text = r"""
#     numeric a = b,
# """
#
# text = r"""
#     proc f() block
#         numeric a = t + 1,
#     unblock
#
#     numeric b = a,
# """
#
# text = r"""
#     proc f(numeric t) block
#         print(t + 1),
#     unblock
#
#     f(),
# """
#
# text = r"""
#     record my_type data block
#         logic val conversion to numeric conv_val_to_num conversion from logic ttt,
#     unblock,
# """
#
# text = r"""
#     proc conv_val_to_num(numeric t) block
#
#     unblock
#
#     record my_type data block
#         logic val conversion to numeric conv_val_to_num conversion to numeric ttt,
#     unblock,
# """
#
# text = r"""
#     record my_type data block
#         numeric,
#     unblock,
# """
#
# text = r"""
#     record my_type data block
#         logic val,
#     unblock,
#
#     my_type vvv,
#     vvv->eee = 4,
# """
#
# text = r"""
#     record my_type data block
#         logic val,
#         string val,
#     unblock,
# """
#
# text = r"""
#
#     if  block
#         print(5+10),
#     unblock,
# """
#
# text = r"""
#     {true} block
#
#     unblock,
# """
#
# text = r"""
#     numeric a = 5,
#     2 = a,
# """
#
# text = r"""
#     moveup(1),
#     pingup(),
#     voice(ss),
#     vision(1, 2),
# """


parser = Parser(yacc_debug=True)
# with open("test1.txt", "r") as f:
#     ast = parser.parse(f.read(), filename='test1.txt', debuglevel=False)
ast = parser.parse(text, filename='<none>', debuglevel=False)
ast.show(attrnames=True, showcoord=True)

exec = nodevisitor.NodeVisitor(parser._err_flag, parser.func_dict)
exec.visit(ast)