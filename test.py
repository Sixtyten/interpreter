
from parser import Parser

import nodevisitor


# text = r"""
#
#
#     proc main() block
#
#         {t ? 2} block
#             t = t + 2,
#         unblock,
#     unblock
# """
# text = r"""
#     record eee data block
#         logic t,
#         numeric m[5],
#     unblock,
#
#
#
# """

# text = r"""
#     logic t = false,
#     logic m = t,
#     m = m ^ t,
#     t = m / t,
# """
#
# text = r"""
#     proc main() block
#
#
#         numeric t = 10,
#
#
#
#         t = t ^ true,
#     unblock
# """
#
# text = r"""
#     numeric t = 2,
#
#     {t ? 2} block
#         t = t + 2,
#     unblock,
#
#     if (t ? 4)
#         t = t + 2,
#
#     t = t / 2,
# """

# text = r"""
#         numeric t = 2,
#
#         {t ? 2} block
#             t = t + 2,
#             print(666),
#         unblock,
#
#         if (t ? 4)
#             t = t + 2,
#
#         t = t / 2,
# """

text = r"""
    record my_array data block
        numeric arr[4],
    unblock,

    record my_type data block
        my_array fff[2][2],
        numeric val,
        string vvv,
    unblock,

    my_type zzz,
    numeric check[4] = zzz->fff[0][1]->arr,
    logic t = zzz->fff[0][1]->arr[2],
    print(zzz->fff[0][1]->arr),
"""
# #
# text = r"""
#     numeric r = 3,
#     numeric t[r][4][3],
#
#     t[0][1][2] = r,
# """

# text = r"""
#     string v = "dqw",
# """
#
# text = r"""
#     undef 4,
#
#     numeric t[4][3],
#
#     undef 10,
#     numeric b[4][3],
#
#     t = t + b,
# """
#
# text = r"""
#         undef "wqs",
#         logic t,
#         numeric ggg,
#         t = 4 +. false,
# """
# text = r"""
#     numeric n = 5,
#     numeric buf = 0,
#     numeric res = 1,
#     proc factorial() block
#         buf = n,
#         if (buf ! 1) block
#             res = res * buf,
#             buf = buf - 1,
#             n = buf,
#             factorial(),
#         unblock,
#     unblock,
#
#     factorial(),
# """
#
# text = r"""
#     proc inc(numeric n &) block
#         n = n + 1,
#     unblock,
#
#     proc test(numeric d &) block
#         numeric fff = 3,
#
#         inc(fff),
#
#         d = d + fff,
#     unblock
#
#     proc test_no_reference(numeric d) block
#         numeric fff = 3,
#
#         inc(fff),
#
#         d = d + fff,
#     unblock
#
#     numeric d = 2,
#     test(d),
#
#     test_no_reference(d),
#     print(d),
# """

#
# text = r"""
#     numeric a = 1,
#     numeric b = 2,
#     numeric c = 3,
#
#     a = b = c,
# """
#



text = r"""
    undef "undef",

    record robot_position data block
        numeric x,
        numeric y,
    unblock,
    
    robot_position pos,
    
    proc init(robot_position pos &) block
        pos->x = 2,
        pos->y = 7,
    unblock
    
    init(pos),
    
    if (map[pos->y][pos->x] ! 0) 
        print("ERROR", map[pos->y][pos->x]),
    
    string r[4],
    
    numeric buf = 1,
    movedown(pos, buf),
    
    vision(pos, r),
    voice(pos, r[2]),
    
    numeric pos_x = pos->x,
    numeric pos_y = pos->y,
"""

text = r"""
    proc in_stack(stack st, robot_position sample,logic res&) block
        robot_position cur,
        { st->head ! 0 } block
            pop(st, cur),
            
        unblock,
    unblock,
"""




# text = r"""
#
#     proc test(numeric arr[2][2]&) block
#         arr = arr + arr,
#     unblock,
#
#     undef 10,
#     numeric arr[2][2],
#     test(arr),
#     print(arr),
# """

# text = r"""
#     record robot_position data block
#         numeric x,
#         numeric y,
#         numeric label,
#     unblock,
#
#     proc init(robot_position pos &, numeric _x, numeric _y) block
#         pos->x = _x,
#         pos->y = _y,
#     unblock
#
#     record stack data block
#         robot_position _data[10],
#         numeric head,
#     unblock,
#
#     proc push(stack st&, robot_position pos) block
#         st->_data[st->head] = pos,
#         st->head = st->head + 1,
#     unblock
#
#     proc test(robot_position pos&) block
#         robot_position fff,
#         init(fff, 6, 66),
#
#         push(pos, fff),
#     unblock,
# """

text = r"""
    proc vbnnn() block
    
    unblock
    
    record my_array data block
        numeric arr[4],
    unblock,



    record my_type data block
        my_array fff[2][2],
        numeric val conversion to vbnnn conversion from cvbvbb,
        
    unblock,
    
    my_array vbn,
"""

text = r"""
    undef true,

    proc conv_val_to_num(array _from &, numeric _to &) block
        _to = _from[0]  *. 1,
    unblock,
    
    

    record my_type data block

        logic val[4] conversion to numeric conv_val_to_num,

    unblock,

    my_type test,

    numeric t =  test->val + .4,
    print(t), 
"""

# text = r"""
#     proc t(numeric a, logic b) block
#         print(a),
#     unblock
#
#     numeric a = 1,
#     logic b = true,
#     t(a, false),
# """

text = r"""
    undef 10,
    
    proc conv_val_to_num(logic _from &, numeric _to &) block
        _to = _from  *. 1,
    unblock,

    record my_type data block
        numeric val conversion from string conv_string_to_val,
        logic boolean conversion to numeric conv_val_to_num, 
    unblock,

    proc conv_string_to_val(string _from &, my_type _to &) block
        _to->val = _from *. 1,
    unblock,
    
    

    my_type test,
    my_type test1,

    logic t =  test1->val .+ test->boolean,
"""

text = r"""
    record robot_position data block
        numeric x,
        numeric y,
        numeric label,
    unblock,

    proc init(robot_position pos &, numeric _x, numeric _y, numeric _label) block
        pos->x = _x,
        pos->y = _y,
        pos->label = _label,
    unblock

    proc is_exit(robot_position pos, logic res&) block
        res = false,
        if (map[pos->y][pos->x] ? 3)
            res = true,
    unblock  

    proc set_label(robot_position pos &, numeric _label) block
        pos->label = _label,
    unblock

    proc are_equal(robot_position left, robot_position right, logic res&) block
        res = false,
        if ((left->x ? right->x) * (left->y ? right->y))
            res = true,
    unblock

    record stack data block
        robot_position _data[100],
        numeric head, 
    unblock,

    record pwds data block
        numeric arr[4],
    unblock,

    record vision_stack data block
        pwds _data[100],
        numeric head,
    unblock,

    proc push(stack st&, robot_position pos) block
        st->_data[st->head] = pos,
        st->head = st->head + 1, 
    unblock

    proc v_push(vision_stack st&, pwds pwd) block
        st->_data[st->head] = pwd,
        st->head = st->head + 1, 
    unblock

    proc pop(stack st&, robot_position res&) block
        if (st->head ? 0)
            print("ERROR: stack is empty"),
        if (st->head ! 0) block
            st->head = st->head - 1,
            res = st->_data[st->head],
        unblock,
    unblock

    proc v_pop(stack st&, pwds res&) block
        if (st->head ? 0)
            print("ERROR: stack is empty"),
        if (st->head ! 0) block
            st->head = st->head - 1,
            res = st->_data[st->head],
        unblock,
    unblock

    proc is_in_stack(stack st, robot_position sample, numeric num, logic res&) block
        res = false,
        robot_position cur,
        logic buf = false,
        numeric cnt = 0,
        { (st->head ! 0) * (res ? false) } block
            pop(st, cur),
            are_equal(cur, sample, buf),
            if (buf ? true) block
                cnt = cnt + 1,
                if (cnt ? num)
                    res = true,
            unblock,
        unblock,
    unblock

    proc get_neighbours(robot_position cur, stack res&) block
        numeric num = 2,
        undef -1,
        pingup(cur, num),
        if (num ? 0) block
            num = 1,
            moveup(cur, num),
            push(res, cur),
            num = 1,
            movedown(cur, num),

        unblock,
        num = 2,
        pingdown(cur, num),
        if (num ? 0) block
            num = 1,
            movedown(cur, num),
            push(res, cur),
            num = 1,
            moveup(cur, num),

        unblock,
        num = 2,
        pingleft(cur, num),
        if (num ? 0) block
            num = 1,
            moveleft(cur, num),
            push(res, cur),

            num = 1,
            moveright(cur, num),

        unblock,
        num = 2,
        pingright(cur, num),
        if (num ? 0) block
            num = 1,
            moveright(cur, num),

            push(res, cur),
            num = 1,
            moveleft(cur, num),

        unblock,
        undef 0,
    unblock

    logic exit_reached = false,
    vision_stack pwds_stack,

    proc mark_neighbours(stack st&, robot_position cur, numeric _label, stack path&) block
        logic log_buf,
        set_label(cur, _label),
        push(st, cur),     
        pwds pw,
        vision(cur, pw->arr),     
        v_push(pwds_stack, pw),       
        robot_position buf,
        stack res,       
        get_neighbours(cur, res),
        {res->head ! 0} block
            pop(res, buf),
            is_in_stack(st, buf, 1, log_buf),
            if (log_buf ? false) block
                marked_map[buf->y][buf->x] = _label + 1,
                is_exit(buf, log_buf),

                if (log_buf ? true)  block
                    exit_reached = true,

                    set_label(buf, buf->label+1),
                    push(st, buf),
                unblock,
                mark_neighbours(st, buf, _label + 1, path),
            unblock,
        unblock,

        if ((path->head ? 0) * (exit_reached ? true)) block
            {st->head ! 0} block
                pop(st, buf),
                if ((path->head ? 0) + (path->_data[path->head - 1]->label - buf->label ? 1))
                    push(path, buf),
            unblock,
        unblock,
    unblock

    proc go_to_next_cell(robot_position cur&, robot_position next) block
        numeric steps = 1,
        logic res = false,
        are_equal(cur, next, res),
        if (res ? false) block
            if ((cur->x - next->x ? 1) * (cur->y - next->y ? 0))
                moveleft(cur, steps),
            if ((cur->x - next->x ? -1) * (cur->y - next->y ? 0))
                moveright(cur, steps),

            if ((cur->x - next->x ? 0) * (cur->y - next->y ? 1))
                moveup(cur, steps),
            if ((cur->x - next->x ? 0) * (cur->y - next->y ? -1))
                movedown(cur, steps),
        unblock,
    unblock

    proc go_to_exit(robot_position pos&) block
        pwds buf,
        undef "",
        string empty[4],
        undef 0,
        numeric cnt,
        {(pwds_stack->head ! 0)} block
            v_pop(pwds_stack, buf),
            cnt = 0,
            {cnt ! 4} block
                voice(pos, buf->arr[cnt]),
                cnt = cnt + 1,
            unblock,
        unblock,
    unblock

    proc proceed_path(stack path&, robot_position pos&) block
        robot_position buf,
        logic res,
        {(pos->x ! -1) * (pos->y ! -1)} block
            pop(path, buf),
            go_to_next_cell(pos, buf),
            is_exit(pos, res),
            if (res ? true) block
                go_to_exit(pos),
            unblock,
        unblock,
    unblock

    undef -1,
    numeric marked_map[9][9],
    undef 0,

    stack test,
    stack path,
    test->head = 0,

    robot_position robot,

    init(robot, 2, 2, -1),
    mark_neighbours(test, robot, 0, path),
    

    proceed_path(path, robot),

    logic res = false,
    if ((robot->x ? -1) * (robot->y ? -1))
        res = true,

    if (res ? true)
        print("SUCCESS"),
    if (res ? false)
        print("FAIL"),
"""


parser = Parser(yacc_debug=True)
# with open("test1.txt", "r") as f:
#     ast = parser.parse(f.read(), filename='test1.txt', debuglevel=False)
ast = parser.parse(text, filename='<none>', debuglevel=False)
ast.show(attrnames=True, showcoord=True)

exec = nodevisitor.NodeVisitor(parser._err_flag, parser.func_dict)
exec.visit(ast)