import tree
import copy
import numpy as np
import passwords


class NodeVisitor:

    def __init__(self, error_flag, func_dict):
        self._error_flag = error_flag

        self._func_dict = func_dict

        self.struct_dict = {}

        self.scopes = {'__global': {}

                       }

        self.operators = {'-'  : lambda x, y: not (x or y) if type(x) == bool and type(y) == bool else x - y,
                          '+'  : lambda x, y: x or y if type(x) == bool and type(y) == bool else x + y,

                          '*'  : lambda x, y: x and y if type(x) == bool and type(y) == bool else x * y,
                          '<'  : lambda x, y: x <   y,
                          '>'  : lambda x, y: x >   y,
                          '<=' : lambda x, y: x <=  y,
                          '>=' : lambda x, y: x >=  y,
                          '?'  : lambda x, y: x ==  y,

                          '!'  : lambda x, y: x !=  y,

                          '^'  : lambda x, y: x ** y if type(x) == int and type(y) == int else not x and not y,
                          '/'  : lambda x, y: x // y if type(x) == int and type(y) == int else not (x and y),

                          }
        self.unary_operators = {'-': lambda x: -x if type(x) == int else not x,

                                }

        self.basic_types = ('logic', 'numeric', 'string')

        self.types_dict = {'logic'  : bool,
                           'numeric': int,
                           'string' : str,

                           bool     : 'logic'  ,
                           int      : 'numeric',
                           str      : 'string' ,
                           np.ndarray : 'array',
                           np.array:    'array'
                           }

        self._undef = 0

        self.scopes['__global']['map'] = ['array', np.array([[2, 2, 2, 2, 2, 2, 2, 2, 2],
                                                             [2, 0, 0, 0, 0, 0, 0, 0, 2],
                                                             [2, 0, 0, 1, 1, 1, 1, 1, 2],
                                                             [2, 0, 0, 0, 0, 1, 0, 0, 2],
                                                             [2, 1, 1, 0, 1, 0, 1, 1, 2],
                                                             [2, 1, 1, 0, 0, 1, 0, 0, 2],
                                                             [2, 1, 1, 0, 0, 1, 0, 0, 3],
                                                             [2, 0, 0, 0, 0, 0, 0, 0, 2],
                                                             [2, 3, 2, 2, 2, 2, 2, 2, 2]])]

        self._move_dict = {'up': lambda x, y: (x, y - 1),
                           'down': lambda x, y: (x, y + 1),
                           'left': lambda x, y: (x - 1, y),
                           'right': lambda x, y: (x + 1, y),

                           }

        self._robot_operators = tuple('move' + el for el in self._move_dict.keys()) + tuple('ping' + el for el in self._move_dict.keys())

        self.scopes['__global']['__keys'] = passwords.keys
        self.scopes['__global']['passwords'] = passwords.pwds
        #self.struct_dict['robot_position'] = {'x': ['numeric', 0, {'to': None, 'from': None}], 'y': ['numeric', 0, {'to': None, 'from': None}]}

        #self.scopes['__global']['_pos'] = ['robot_position', {'x': ['numeric', 0, {'to': None, 'from': None}], 'y': ['numeric', 0, {'to': None, 'from': None}]}]

    def _cast_undef(self, type, val):
        try:
            if type == 'logic':
                return bool(val)
            elif type == 'numeric':
                return int(val)
            elif type == 'string':
                return str(val)
        except (ValueError, TypeError):
            print("val :", val)
            if type == 'numeric':
                return 0

            return None

    def visit(self, node, scope_name='__global'):
        method = 'visit_' + node.__class__.__name__
        return getattr(self, method, self.generic_visit)(node, scope_name=scope_name)

    def generic_visit(self, node, scope_name='__global'):
        if node is None:
            return None
        else:
            return (self.visit(c, scope_name=scope_name) for c_name, c in node.children())

    def visit_Constant(self, n, scope_name='__global'):
        if n.type == 'logic':
            return bool(n.value)
        if n.type == 'string':
            return n.value
        return int(n.value)

    def visit_ID(self, n, scope_name='__global', get_const=False):
        ref = self.scopes[scope_name].get(n.name)
        if ref is None:
            ref = self.scopes['__global'].get(n.name)
            if not ref:
                print("Error at {}: unresolved reference to {}".format(n.coord, n.name))
                self._error_flag = True
                return None
        return ref[1] if get_const else n.name

    def visit_StructRef(self, n, scope_name='__global', get_conversions=False, get_type=False, get_struct_type=False):
        fields = []
        try:
            if get_struct_type:
                return self.scopes[scope_name][n.name.name][0]
            while type(n.name) != str:
                if type(n) == tree.ArrayRef:
                    last = self.visit_ArrayRef(n, scope_name)
                    break
                fields.append(n.field.name)
                n = n.name

            if type(n) != tree.ArrayRef:
                if self.scopes[scope_name].get(n.name) is None:
                    last = self.scopes['__global'][n.name][1]
                else:
                    last = self.scopes[scope_name][n.name][1]
            for index, field in enumerate(reversed(fields)):
                if index == len(fields) - 1:
                    if get_conversions:
                        last = last[field][2]
                    elif get_type:
                        last = last[field][0]
                    else:
                        last = last[field][1]
                    continue
                last = last[field][1]
        except (KeyError, TypeError):
            print("Error at {}: invalid reference to {}".format(n.coord, n.name))
            self._error_flag = True
            return
        return last

    def _build_print_args(self, name, arr, scope_name, n, convert_to_str=True):
        if type(name) == list or type(name) == np.ndarray:
            arr.append(str(name))
            return

        ref = self.scopes[scope_name].get(name)
        if ref is None:
            ref = self.scopes['__global'].get(name)
            if ref is None:
                print("Error at {}: unresolved reference to {}".format(n.coord, name))
                self._error_flag = True
                return
        if convert_to_str:
            if type(ref) == list:
                arr.append(str(ref[1]))
            else:
                arr.append(str(ref))
        else:
            arr.append(ref[1])

    def _ping(self, name, ty, move, scope_name='__global'):
        if ty == 1:
            target = (2, 1)
        if ty == 2:
            target = (0, 3)
        elif ty == 3:
            target = [3]

        pos_x = self.scopes[scope_name][name][1]['x'][1]
        pos_y = self.scopes[scope_name][name][1]['y'][1]

        res = 0
        try:

            pos_x, pos_y = self._move_dict[move](pos_x, pos_y)
            while self.scopes['__global']['map'][1][pos_y][pos_x] not in target:
                pos_x, pos_y = self._move_dict[move](pos_x, pos_y)
                res += 1
            return res

        except:
            return self._cast_undef('numeric', self._undef)

    def _move(self, name, steps, move, scope_name='__global'):
        pos_x = self.scopes[scope_name][name][1]['x'][1]
        pos_y = self.scopes[scope_name][name][1]['y'][1]
        cnt = 0
        try:
            for _ in range(steps):
                pos_x, pos_y = self._move_dict[move](pos_x, pos_y)
                if self.scopes['__global']['map'][1][pos_y][pos_x] not in (0, 3):
                    return steps - cnt
                self.scopes[scope_name][name][1]['x'][1] = pos_x
                self.scopes[scope_name][name][1]['y'][1] = pos_y
                cnt += 1
            return 0

        except:
            return steps - cnt

    def _vision(self, name, scope_name='__global'):
        pos_x = self.scopes[scope_name][name][1]['x'][1]
        pos_y = self.scopes[scope_name][name][1]['y'][1]

        return self.scopes['__global']['passwords'][1][pos_y][pos_x]

    def _voice(self, name, pwd, scope_name='__global'):
        pos_x = self.scopes[scope_name][name][1]['x'][1]
        pos_y = self.scopes[scope_name][name][1]['y'][1]

        correct = self.scopes['__global']['__keys'][1][pos_y][pos_x]

        return correct == pwd

    def visit_FuncCall(self, n, scope_name='__global'):
        if n.name.name == 'print':
            val = self.visit(n.args, scope_name=scope_name)
            s = []
            if type(val) == list:
                for i, item in enumerate(val):
                    if item is None:
                        return
                    if type(item) in (int, bool, dict, np.int64, np.bool_):
                        s.append(str(item))
                        continue

                    if type(n.args.exprs[i]) != tree.ID and type(item) == str:
                        s.append(item)

                    else:
                        self._build_print_args(item, s, scope_name, n)

            elif type(val) == str:
                if type(n.args.exprs[0]) == tree.ID:
                    self._build_print_args(val, s, scope_name, n)
                else:
                    s.append(val)
            else:
                s.append(str(val))
            if not self._error_flag:
                print("PRINT CALL: ", ', '.join(s))
        elif n.name.name in self._robot_operators:
            prefix = n.name.name[:4]
            direction = n.name.name[4:]
            try:
                val = self.visit_ID(n.args.exprs[1], get_const=True, scope_name=scope_name)
                if val <= 0:
                    print("Error at {}: second parameter of system procedure {} must be > 0".format(n.coord, n.name.name))
                    self._error_flag = True
                    return
                res = getattr(self, '_' + prefix)(n.args.exprs[0].name, val, direction, scope_name)

                self.scopes[scope_name][n.args.exprs[1].name][1] = res
            except :
                print("Error at {}: invalid call of system procedure {}".format(n.coord, n.name.name))
                self._error_flag = True
                return
        elif n.name.name == 'voice':
            try:
                if type(n.args.exprs[1]) == tree.ID:
                    val = self.visit_ID(n.args.exprs[1], get_const=True, scope_name=scope_name)
                else:
                    val = self.visit(n.args.exprs[1], scope_name=scope_name)

                if self._voice(n.args.exprs[0].name, val, scope_name):
                    self.scopes[scope_name][n.args.exprs[0].name][1]['x'][1] = -1
                    self.scopes[scope_name][n.args.exprs[0].name][1]['y'][1] = -1
            except:
                print("Error at {}: invalid call of system procedure {}".format(n.coord, n.name.name))
                self._error_flag = True
                return

        elif n.name.name == 'vision':
            try:

                res = self._vision(n.args.exprs[0].name, scope_name)
                if type(n.args.exprs[1]) == tree.ID:
                    arr = self.visit_ID(n.args.exprs[1], scope_name, get_const=True)
                else:
                    arr = self.visit(n.args.exprs[1], scope_name)
                if arr.shape != np.array([1, 1, 1, 1]).shape:
                    print("Error at {}: target array {} must have shape (1, 4)".format(n.coord, n.args.exprs[1].name))
                    self._error_flag = True
                    return
                if type(n.args.exprs[1]) == tree.StructRef:
                    to_visit = tree.Assignment("=", n.args.exprs[1], tree.Constant("string", res))
                    self.visit_Assignment(to_visit, scope_name)
                else:
                    self.scopes[scope_name][n.args.exprs[1].name][1] = res
            except:
                print("Error at {}: invalid call of system procedure {}".format(n.coord, n.name.name))
                self._error_flag = True
                return
        else:
            old_scope = scope_name
            if not self._error_flag:
                try:
                    args_values = self.visit(n.args, scope_name=scope_name)
                    if self._error_flag:
                        return
                    scope_name = n.name.name
                    i = 1
                    if self.scopes.get(scope_name + str(i)) is None:
                        self.scopes[scope_name + str(i)] = copy.deepcopy(self.scopes[scope_name])
                    else:
                        while self.scopes.get(scope_name + str(i)) is not None:
                            i += 1
                        self.scopes[scope_name + str(i)] = copy.deepcopy(self.scopes[scope_name])
                    scope_name = scope_name + str(i)
                    s = []
                    if args_values:

                        for pos, arg in enumerate(args_values):
                            ty = self.scopes[n.name.name][self._func_dict[n.name.name].decl.type.args.params[pos].type.declname][0]

                            if isinstance(arg, (bool, int)):
                                s.append(arg)

                            elif type(arg) == str:
                                if type(ty) == tree.IdentifierType:
                                    ty = ty.names[0]
                                if ty[-1] == '&' or ty == 'string':
                                    s.append(arg)
                                else:
                                    self._build_print_args(arg, s, old_scope, n, convert_to_str=False)
                            else:
                                self._build_print_args(arg, s, old_scope, n, convert_to_str=False)

                        for j, item in enumerate([val.type.declname for val in self._func_dict[n.name.name].decl.type.args.params]):
                            if type(self.scopes[n.name.name][self._func_dict[n.name.name].decl.type.args.params[j].type.declname][0]) == tree.IdentifierType:
                                self.scopes[n.name.name][
                                    self._func_dict[n.name.name].decl.type.args.params[j].type.declname][0] = self.scopes[n.name.name][self._func_dict[n.name.name].decl.type.args.params[j].type.declname][0].names[0]

                            if self.scopes[n.name.name][self._func_dict[n.name.name].decl.type.args.params[j].type.declname][0][-1] == '&':
                                    if self.scopes[old_scope].get(s[j]) is None:
                                        self.scopes[scope_name][item] = self.scopes['__global'][s[j]]
                                    else:
                                        self.scopes[scope_name][item] = self.scopes[old_scope][s[j]]
                            else:
                                self.scopes[scope_name][item][1] = copy.deepcopy(s[j])
                except:
                    print("Error at {}: invalid function {} call".format(n.coord, n.name.name))
                    self._error_flag = True
                    return

                self.visit_Compound(self._func_dict[n.name.name].body, scope_name, execute_flag=True)

                del self.scopes[scope_name]

    def visit_UnaryOp(self, n, scope_name='__global'):

        if n.op == 'undef':
            if not self._error_flag:
                self._undef = self.visit(n.expr)
                return
            return
        return self.unary_operators[n.op](self.visit(n.expr))

    def _converse_to(self, n, func_name, from_operand, to_type):
        self.scopes['__global']['__tmp_from'] = [self.types_dict[type(from_operand)], from_operand]
        self.scopes['__global']['__tmp_to'] = [to_type + '&', self._cast_undef(to_type, self._undef)]
        expr_list = tree.ExprList([tree.ID('__tmp_from'), tree.ID('__tmp_to')], n.coord)
        func_call = tree.FuncCall(tree.ID(func_name), expr_list, n.coord)
        self.visit_FuncCall(func_call)
        res = self.scopes['__global']['__tmp_to'][1]
        del self.scopes['__global']['__tmp_to']
        del self.scopes['__global']['__tmp_from']
        return res

    def _converse_from(self, n, func_name, from_type, from_value, to_operand, to_type, conversions):
        self.scopes['__global']['__tmp_from'] = [from_type + '&', from_value]
        self.scopes['__global']['__tmp_to'] = [to_type, to_operand, conversions]
        expr_list = tree.ExprList([tree.ID('__tmp_from'), tree.ID('__tmp_to')], n.coord)
        func_call = tree.FuncCall(tree.ID(func_name), expr_list, n.coord)
        self.visit_FuncCall(func_call)
        res = self.scopes['__global']['__tmp_to']

        del self.scopes['__global']['__tmp_from']
        return res


    def visit_BinaryOp(self, n, scope_name='__global'):

        ref_l = lvalue = self.visit(n.left, scope_name=scope_name)
        if type(lvalue) == str and type(n.left) != tree.Constant:
            ref_l = self.visit_ID(n.left, scope_name, get_const=True)

        ref_r = rvalue = self.visit(n.right, scope_name=scope_name)
        if type(rvalue) == str and type(n.right) != tree.Constant:
            ref_r = self.visit_ID(n.right, scope_name, get_const=True)

        if tree.Constant in (n.left, n.right) and n.right.type != n.left.type and not '.' in n.op:
            print("Error at {}: cannot make {} for {} and {}".format(n.coord, n.op, n.left.type, n.right.type))
            self._error_flag = True
            return
        try:
            old_operator = n.op
            if n.op[0] == '.':  # cast to left operand
                if type(n.right) == tree.StructRef :
                    conversions = self.visit_StructRef(n.right, scope_name, get_conversions=True)
                    to_type = self.types_dict[type(ref_l)]
                    ref_r = self._converse_to(n, conversions["to"][to_type], ref_r, to_type)
                else:
                    if type(n.left) == tree.StructRef:
                        from_type = self.types_dict[type(ref_r)]
                        from_value = ref_r
                        to_type = self.visit_StructRef(n.left, scope_name=scope_name, get_struct_type=True)
                        conversions = self.visit_StructRef(n.left, scope_name, get_conversions=True)
                        to_operand = copy.deepcopy(self.scopes[scope_name][n.left.name.name][1])
                        self._converse_from(n, conversions["from"][from_type], from_type, from_value, to_operand, to_type, conversions)
                        new_ref = copy.deepcopy(n.left)
                        new_ref.name.name = '__tmp_to'
                        ref_r = self.visit_StructRef(new_ref)
                        del self.scopes['__global']['__tmp_to']
                    else:
                        ref_r = self._cast_operands(from_op=ref_r, to_op=ref_l)

                n.op = n.op[1:]
            elif n.op[-1] == '.':  # cast to right operand
                if type(n.left) == tree.StructRef:
                    conversions = self.visit_StructRef(n.left, scope_name, get_conversions=True)
                    to_type = self.types_dict[type(ref_r)]
                    ref_l = self._converse_to(n, conversions["to"][to_type], ref_l, to_type)
                else:
                    if type(n.right) == tree.StructRef:
                        from_type = self.types_dict[type(ref_l)]
                        from_value = ref_l
                        to_type = self.visit_StructRef(n.right, scope_name=scope_name, get_struct_type=True)
                        conversions = self.visit_StructRef(n.right, scope_name, get_conversions=True)
                        to_operand = copy.deepcopy(self.scopes[scope_name][n.right.name.name][1])
                        self._converse_from(n, conversions["from"][from_type], from_type, from_value, to_operand, to_type, conversions)
                        new_ref = copy.deepcopy(n.right)
                        new_ref.name.name = '__tmp_to'
                        ref_l = self.visit_StructRef(new_ref)
                        del self.scopes['__global']['__tmp_to']
                    else:
                        ref_l = self._cast_operands(from_op=ref_l, to_op=ref_r)
                n.op = n.op[:-1]
            if type(ref_l) != np.ndarray and type(ref_r) != np.ndarray and None in (ref_l, ref_r):
                print("Error at {}: invalid binary expression".format(n.coord))
                self._error_flag = True
                return

            result = self.operators[n.op](ref_l, ref_r)
            n.op = old_operator
            return result
        except (ValueError, KeyError):
        #except NotImplementedError:
            print("Error at {}: cannot calculate binary expression".format(n.coord))
            self._error_flag = True

    def _cast_operands(self, from_op, to_op):
        try:
            if type(from_op) == str:
                from_op = from_op[1:-1]
            if type(to_op) == str:
                from_op = str(from_op)
            elif type(to_op) == int:
                from_op = int(from_op)
            elif type(to_op) == bool:
                from_op = bool(from_op)
            return from_op
        except ValueError:
            if type(to_op) == int:
                return 0

    def visit_ArrayRef(self, n, scope_name='__global'):
        indices = []
        while type(n.name) != str:
            if type(n) == tree.StructRef:
                ar = self.visit_StructRef(n, scope_name)
                break
            if type(n.subscript) == tree.ID:
                indices.append(self.visit_ID(n.subscript, scope_name=scope_name, get_const=True))

            else:
                indices.append(self.visit(n.subscript, scope_name=scope_name))
            n = n.name

        #TODO
        # if self.scopes[scope_name][n.name][0] != 'array':
        #     print("Error at {}: cannot reference to {}".format(n.coord, self.scopes[scope_name][n.name][0]))
        #     self._error_flag = True
        #     return

        if type(n) != tree.StructRef:
            ar = self.visit_ID(n, scope_name, get_const=True)
            #ar = self.scopes[scope_name][n.name][1]
        try:
            for index in reversed(indices):
                ar = ar[index]
        except (IndexError, TypeError):
            print("Error at {}: invalid array reference".format(n.coord))
            self._error_flag = True
            return

        if type(ar) == list:
            print("Error at {}: invalid array reference".format(n.coord))
            self._error_flag = True
            return

        return ar

    def visit_Assignment(self, n, scope_name='__global'):

        rvalue = self.visit(n.rvalue, scope_name=scope_name)
        if type(rvalue) == str and rvalue[0] != '"' and rvalue[-1] != '"':
            rvalue = self.visit_ID(n.rvalue, scope_name, get_const=True)

        if type(n.lvalue) == tree.ArrayRef:

            indices = []
            cur = n.lvalue
            while type(cur.name) != str:
                if type(cur) == tree.StructRef:
                    res = self.visit_StructRef(cur, scope_name)
                    break
                if type(cur.subscript) == tree.ID:
                    indices.append(self.visit_ID(cur.subscript, scope_name=scope_name, get_const=True))
                else:
                    indices.append(self.visit(cur.subscript, scope_name=scope_name))
                cur = cur.name
            if type(cur) != tree.StructRef:
                ref = self.scopes[scope_name].get(cur.name)
                if not ref:
                    ref = self.scopes['__global'].get(cur.name)
                    if not ref:
                        print("Error at {}: unresolved reference to {}".format(n.coord, cur.name))
                        self._error_flag = True
                        return

                if ref[0] != 'array':
                    print("Error at {}: invalid array reference to {}".format(n.coord, cur.name))
                    self._error_flag = True
                    return

                res = ref[1]

            if type(n.rvalue) == tree.ID:
                rvalue = self.visit_ID(n.rvalue, scope_name, True)
            else:
                rvalue = self.visit(n.rvalue, scope_name)

            if type(res) == list and type(res[0]) == list and len(res) == 1 and len(indices) == 1:
                res = res[0]

            copy_ind = copy.copy(indices)
            for index in reversed(indices):
                try:
                    last = res
                    res = res[index]
                    copy_ind.pop(-1)
                    if type(res) == int:
                        if type(rvalue) == list:
                            print("Error at {}: invalid index".format(n.coord))
                            self._error_flag = True
                            return
                        if not self._error_flag:
                            if len(copy_ind) > 0:
                                print("Error at {}: invalid index".format(n.coord))
                                self._error_flag = True
                                return
                            last[index] = rvalue
                        return rvalue

                except (IndexError, TypeError):
                    print("Error at {}: invalid index".format(n.coord))
                    self._error_flag = True
                    return

            # if len(last[indices[0]]) != rvalue:
            #     print("Error at {}: invalid array".format(n.coord))
            #     self._error_flag = True
            #     return
            if not self._error_flag:

                last[indices[0]] = rvalue
            return rvalue

        elif type(n.lvalue) == tree.StructRef:
            fields = []
            cur = n.lvalue
            try:
                while type(cur.name) != str:
                    if type(cur) == tree.ArrayRef:
                        last = self.visit_ArrayRef(cur, scope_name)
                        break
                    fields.append(cur.field.name)
                    cur = cur.name

                if type(cur) != tree.ArrayRef:
                    last = self.scopes[scope_name][cur.name][1]
                if len(fields) > 1:
                    for pos, field in enumerate(reversed(fields)):
                        if pos == len(field) - 2:
                            break
                        last = last[field][1]

                last[fields[0]][1] = rvalue
            except (KeyError, TypeError):
                print("Error at {}: invalid struct reference in assignment".format(n.coord))
                self._error_flag = True
                return

            return rvalue

        lvalue = self.visit(n.lvalue, scope_name=scope_name)


        try:
            ref = self.scopes[scope_name].get(lvalue)
        except TypeError:
            print("Error at {}: invalid lvalue operand".format(n.coord))
            self._error_flag = True
            return

        if not ref:
            ref = self.scopes['__global'].get(lvalue)
            if not ref:
                print("Error at {}: unresolved reference to {}".format(n.coord, lvalue))
                self._error_flag = True
                return

        ref[1] = rvalue

        return rvalue

    def _visit_expr(self, n, scope_name='__global'):
        return self.visit(n, scope_name=scope_name)

    def visit_Struct(self, n, scope_name='__global'):
        if self.struct_dict.get(n.name):
            print("Error at {}: redefinition of record {}".format(n.coord, n.name))
            self._error_flag = True
            return

        self.struct_dict[n.name] = {}
        for item in n.decls:
            self.visit_Decl(item, n.name, True)

    def visit_ArrayDecl(self, n, scope_name='__global'):
        sizes = []
        while type(n) == tree.ArrayDecl:
            if type(n.dim) == tree.ID:
                dim = self.visit_ID(n.dim, scope_name, True)
            else:
                dim = self.visit(n.dim, scope_name)

            if type(dim) != int:
                self._error_flag = True
                return
            sizes.append(dim)
            n = n.type

        duplicate = []
        for i in reversed(sizes):
            if duplicate == []:
                if type(n.type.names[0]) == tree.IdentifierType:
                    val = self.struct_dict.get(n.type.names[0].names[0])

                else:
                    val = self._cast_undef(n.type.names[0], self._undef)
                if val is None:
                    print("Error at {}: invalid array declaration".format(n.coord))
                    self._error_flag = True
                    return
                for _ in range(i):
                    duplicate.append(copy.deepcopy(val))
                continue

            duplicate = [copy.deepcopy(duplicate)]
            for _ in range(i - 1):
                duplicate.append(copy.deepcopy(duplicate[0]))

        return np.array(duplicate, dtype=object)

    def visit_Decl(self, n, scope_name='__global', in_struct=False):
        if in_struct:
            if self.struct_dict[scope_name].get(n.name):
                print("Error at {}: redefinition of field {} in record {}".format(n.coord, n.name, scope_name))
                self._error_flag = True
                return

            if type(n.type) == tree.ArrayDecl:
                val = self.visit_ArrayDecl(n.type, scope_name=scope_name)
                if val is None:
                    print("Error at {}: unresolved reference to variable {}".format(n.coord, n.init.name))
                    self._error_flag = True
                    return

                self.struct_dict[scope_name][n.name] = ["array", copy.deepcopy(val), {"to": n.conv_to, "from": n.conv_from}]

            elif type(n.type) == tree.TypeDecl:
                if type(n.type.type.names[0]) == tree.IdentifierType:
                    val = self.struct_dict.get(n.type.type.names[0].names[0])
                    if val is None:
                        print("Error at {}: cannot cast type {}".format(n.coord, n.type.type.names[0]))
                        self._error_flag = True
                        return
                    # TODO uncomment this
                    # self.struct_dict[scope_name][n.name] = [n.type.type.names[0].names[0], copy.deepcopy(val), {"to": n.conv_to, "from": n.conv_from}]
                    self.struct_dict[scope_name][n.name] = [n.type.type.names[0].names[0], copy.deepcopy(val)]
                    return

                val = self._cast_undef(n.type.type.names[0], self._undef)
                if val is None:

                    print("Error at {}: cannot cast type {}".format(n.coord, n.type.type.names[0]))
                    self._error_flag = True
                    return

                self.struct_dict[scope_name][n.name] = [n.type.type.names[0], copy.deepcopy(val), {"to": n.conv_to, "from": n.conv_from}]

            return

        if self.scopes[scope_name].get(n.name):
            print("Error at {}: redefinition of variable {} in {} scope".format(n.coord, n.name, scope_name))
            self._error_flag = True
            return
        val = self.visit(n.init, scope_name=scope_name)

        if val is None:
            if type(n.type) == tree.Struct:
                self.visit(n.type, scope_name)
                return
            elif type(n.type) == tree.ArrayDecl:
                val = self.visit_ArrayDecl(n.type, scope_name=scope_name)
                if val is None:
                    if n.init:
                        print("Error at {}: unresolved reference to variable {}".format(n.coord, n.init.name))
                    self._error_flag = True
                    return

                self.scopes[scope_name][n.name] = ["array", val]
                return
            elif type(n.type.type.names[0]) == str:
                val = self._cast_undef(n.type.type.names[0], self._undef)
                if val is None:
                    print("Error at {}: cannot cast undef".format(n.coord))
                    self._error_flag = True
                    return

                self.scopes[scope_name][n.name] = [n.type.type.names[0], val]

            else:
                if self.scopes[scope_name].get(n.name):
                    print("Error at {}: redefinition of variable {} in {} scope".format(n.coord, n.name, scope_name))
                    self._error_flag = True
                    return
                self.scopes[scope_name][n.name] = [n.type.type.names[0].names[0], copy.deepcopy(self.struct_dict[n.type.type.names[0].names[0]])]
                return

        if type(n.init) == tree.ID:
            if self.scopes[scope_name].get(n.init.name) is None:
                if self.scopes['__global'].get(n.init.name) is None:
                    print("Error at {}: unresolved reference to variable {}".format(n.coord, n.init.name))
                    self._error_flag = True
                else:
                    val = self.scopes['__global'][n.init.name][1]

            elif n.type.type.names[0] != self.scopes[scope_name][n.init.name][0]:
                print("Error at {}: type {} doesn't correlate with {}".format(n.coord, n.type.type.names[0], n.init.name))
                self._error_flag = True
            else:
                n.type.type.names[0] = self.scopes[scope_name][n.init.name][0]
                val = self.scopes[scope_name][n.init.name][1]

        elif type(n.type) == tree.ArrayDecl:
            if type(val) == int or self.visit_ArrayDecl(n.type, scope_name=scope_name).shape != val.shape:
                print("Error at {}: incorrect array declaration".format(n.coord))
                self._error_flag = True
                return
            self.scopes[scope_name][n.name] = ['array', val]
            return

        elif n.type.type.names[0] in self.basic_types:
            if n.init is None:
                val = self._cast_undef(n.type.type.names[0], val)
                if val is None:
                    print("Error at {}: cannot cast {}".format(n.coord, n.type.type.names[0]))
                    self._error_flag = True
                    return
            else:
                if type(n.init) == tree.ID:
                    val = self.visit_ID(n.init, scope_name=scope_name, get_const=True)
                    val = self._cast_undef(self.scopes[scope_name][n.init.name][0], val)

                if not self._error_flag:
                    val = self._cast_undef(n.type.type.names[0], val)
                    if val is None:
                        print("Error at {}: cannot cast {}".format(n.coord, n.type.type.names[0]))
                        self._error_flag = True
                        return

        elif type(n.init) == tree.ArrayRef:
                if type(val) == list:
                    print("Error at {}: incorrect declaration of {}".format(n.coord, n.name))
                    self._error_flag = True
                    return

        self.scopes[scope_name][n.name] = [n.type.type.names[0], val]

    def visit_ExprList(self, n, scope_name='__global'):
        visited_subexprs = []
        for expr in n.exprs:
            visited_subexprs.append(self._visit_expr(expr, scope_name=scope_name))
        return visited_subexprs

    def visit_FuncDef(self, n, scope_name='__global'):
        self.scopes[n.decl.name] = {}
        if n.decl.type.args:
            for item in n.decl.type.args.params:  # Add parameters to function scope
                if type(item.type.type) == tree.ArrayDecl:

                    if item.type.type.type.type.names[0][-1] == '&':
                        self.scopes[n.decl.name][item.type.type.type.declname] = ["array&", None]
                    else:
                        self.scopes[n.decl.name][item.type.type.type.declname] = ["array", None]
                else:
                    self.scopes[n.decl.name][item.type.declname] = [item.type.type.names[0], None]

        exec_flag = False

        self.visit_Compound(n.body, func_name=n.decl.name, execute_flag=exec_flag)
        if not exec_flag:
            for item in copy.copy(self.scopes[n.decl.name]).keys():
                if n.decl.type.args is not None:

                    if item not in [val.type.declname for val in n.decl.type.args.params]:
                        del self.scopes[n.decl.name][item]
                else:
                    del self.scopes[n.decl.name][item]

    def visit_FileAST(self, n, scope_name='__global'):

        for ext in n.ext:

            self.visit(ext)

        # print("scopes: ", self.scopes)
        # print("structs:", self.struct_dict)

    def visit_Compound(self, n, func_name=None, execute_flag=False):

        if n.block_items:
            for item in n.block_items:
                if execute_flag and not self._error_flag:
                    if type(item) == tree.If:
                        res = self.visit_If(item, scope_name=func_name, execute_flag=execute_flag)
                        if res is not None:
                            return res
                    elif type(item) == tree.While:
                        res = self.visit_While(item, scope_name=func_name)
                        if res is not None:
                            return res
                    elif type(item) == tree.FuncCall or type(item) == tree.Assignment:
                        self.visit(item, scope_name=func_name)

                if not isinstance(item, (tree.If, tree.While, tree.FuncCall, tree.Assignment)):
                    self.visit(item, func_name)

    def visit_EmptyStatement(self, n, scope_name='__global'):
        return

    def visit_ParamList(self, n, scope_name='__global'):
        return [].extend(self.visit(param) for param in n.params)

    def visit_If(self, n, scope_name='__global', execute_flag=False):
        if self._error_flag or not execute_flag:
            pass

        if self.visit(n.cond, scope_name=scope_name):
            if type(n.iftrue) == tree.Compound:
                return self.visit_Compound(n.iftrue, scope_name, True)
            else:
                return self.visit(n.iftrue, scope_name)

    def visit_While(self, n, scope_name='__global'):
        cnt = 0
        while self.visit(n.cond, scope_name=scope_name):
            if cnt == 1000000:
                print("Runtime error at {}".format(n.coord))
                self._error_flag = True
                return
            cnt += 1
            if type(n.stmt) == tree.Compound:
                self.visit_Compound(n.stmt, func_name=scope_name, execute_flag=True)
            else:
                self.visit(n.stmt, scope_name)
